<?php

namespace App\Controller;

use App\Entity\Product;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class YmlController extends AbstractController
{

    /**
     * @Route("/yml", name="yml")
     */
    public function index(EntityManagerInterface $em)
    {
        $catalog = $em->createQueryBuilder()
            ->select('c.id, c.name, c.slug, 
                p.id as product_id, 
                p.name as product_name, p.price, p.announce, p.warranty, p.pic,
                p.vendorModel, p.vendorCode, 
                v.name as vendor_name')
            ->from('App:Category', 'c')
            ->innerJoin('c.products', 'p')
            ->innerJoin('p.vendor', 'v')
            ->where('p.status = :status')->setParameter('status',Product::STATUS_STOCK)
            ->andWhere('p.isActive = :is_active')->setParameter('is_active',true)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY)
        ;

        $categories = [];
        $products = [];

        foreach ($catalog as $item) {
            $categories[$item['id']] = $item['name'];
            $products[$item['product_id']] = [
                'name' => $item['product_name'],
                'price' => $item['price'],
                'vendor' => $item['vendor_name'],
                'vendor_model' => $item['vendorModel'],
                'vendor_code' => $item['vendorCode'],
                'category_id' => $item['id'],
                'price' => $item['price'],
                'warranty' => $item['warranty'],
                'announce' => $item['announce'],
                'picture' => 'http://mikotek.ru/uploads/pics/'.$item['pic'],
                'url' => $this->generateUrl('product_show',[
                    'slug' => $item['slug'], 'id' => $item['product_id']
                ], UrlGeneratorInterface::ABSOLUTE_URL),
            ];
        }

        return $this->render('yml/price-ru.html.twig', [
            'today' => date("Y-m-d H:i"),
            'categories' => $categories,
            'products' => $products,
        ]);
    }

    /**
     * @Route("/priceok/yml", name="priceok_yml")
     */
    public function priceok(EntityManagerInterface $em)
    {
        $catalog = $em->createQueryBuilder()
            ->select('c.id, c.name, c.slug, 
                p.id as product_id, 
                p.name as product_name, p.full_name as product_full_name, p.price, p.announce, p.warranty, p.pic,
                p.vendorModel, p.vendorCode,
                v.name as vendor_name')
            ->from('App:Category', 'c')
            ->innerJoin('c.products', 'p')
            ->innerJoin('p.vendor', 'v')
            ->where('p.status = :status')->setParameter('status',Product::STATUS_STOCK)
            ->andWhere('p.isActive = :is_active')->setParameter('is_active',true)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY)
        ;

        $categories = [];
        $products = [];

        foreach ($catalog as $item) {
            $categories[$item['id']] = $item['name'];
            $products[$item['product_id']] = [
                'name' => $item['product_name'],
                'full_name' => $item['product_full_name'],
                'price' => $item['price'],
                'vendor' => $item['vendor_name'],
                'vendor_model' => $item['vendorModel'],
                'vendor_code' => $item['vendorCode'],
                'category_id' => $item['id'],
                'warranty' => $item['warranty'],
                'announce' => $item['announce'],
                'picture' => 'http://mikotek.ru/uploads/pics/'.$item['pic'],
                'url' => $this->generateUrl('product_show',[
                    'slug' => $item['slug'], 'id' => $item['product_id']
                ], UrlGeneratorInterface::ABSOLUTE_URL),
            ];
        }

        return $this->render('yml/priceok.html.twig', [
            'today' => date("Y-m-d H:i"),
            'categories' => $categories,
            'products' => $products,
        ]);
    }

    /**
     * @Route("/yandex/yml", name="yandex_yml")
     */
    public function yandex(EntityManagerInterface $em)
    {
        $catalog = $em->createQueryBuilder()
            ->select('c.id, c.name, c.fullName as category_name, c.slug, 
                p.id as product_id, 
                p.name as product_name, p.price, p.sku,
                p.stockQty, 
                p.announce, p.warranty, p.pic,
                p.vendorModel, p.vendorCode, 
                p.countryOfOrigin as country_of_origin,
                v.name as vendor_name')
            ->from('App:Category', 'c')
            ->innerJoin('c.products', 'p')
            ->innerJoin('p.vendor', 'v')
            ->where('p.status = :status')->setParameter('status',Product::STATUS_STOCK)
            ->andWhere('p.isActive = :is_active')->setParameter('is_active',true)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY)
        ;

        $categories = [];
        $products = [];

        foreach ($catalog as $item) {
            if (empty($item['sku'])) {
                continue;
            }

            $categories[$item['id']] = $item['category_name'];
            $products[$item['product_id']] = [
                'name' => $item['product_name'],
                'sku' => $item['sku'],
                'price' => $item['price'],
                'stockQty' => $item['stockQty'],
                'country_of_origin' => $item['country_of_origin'],
                'vendor' => $item['vendor_name'],
                'vendor_model' => $item['vendorModel'],
                'vendor_code' => $item['vendorCode'],
                'category_id' => $item['id'],
                'warranty' => $item['warranty'],
                'announce' => $item['announce'],
                'picture' => 'http://mikotek.ru/uploads/pics/'.$item['pic'],
                'url' => $this->generateUrl('product_show',[
                    'slug' => $item['slug'], 'id' => $item['product_id']
                ], UrlGeneratorInterface::ABSOLUTE_URL),
            ];
        }

        return $this->render('yml/yandex.html.twig', [
            'today' => date("Y-m-d H:i"),
            'categories' => $categories,
            'products' => $products,
        ]);
    }

    /**
     * @Route("/sitemap.xml", name="sitemap")
     */
    public function sitemap(EntityManagerInterface $em)
    {
        $catalog = $em->createQueryBuilder()
            ->select('c.id, c.name, c.slug, 
                p.id as product_id, 
                p.name as product_name, p.full_name, p.price, p.announce, p.warranty, p.pic,
                p.vendorModel, p.vendorCode, 
                v.name as vendor_name')
            ->from('App:Category', 'c')
            ->innerJoin('c.products', 'p')
            ->innerJoin('p.vendor', 'v')
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY)
        ;

        $categories = [];
        $products = [];

        foreach ($catalog as $item) {
            $categories[$item['id']] = $item['name'];
            $products[$item['product_id']] = [
                'name' => $item['product_name'],
                'full_name' => $item['full_name'],
                'url' => $this->generateUrl('product_show',[
                    'slug' => $item['slug'], 'id' => $item['product_id']
                ], UrlGeneratorInterface::ABSOLUTE_URL),
            ];
        }
        $page_routes = [
            'payment' => $this->generateUrl('page_view_payment', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'delivery' => $this->generateUrl('page_view_delivery', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'warranty' => $this->generateUrl('page_view_warranty', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'contacts' => $this->generateUrl('page_view_contacts', [], UrlGeneratorInterface::ABSOLUTE_URL),
//            'about' => $this->generateUrl('page_view_about', UrlGeneratorInterface::ABSOLUTE_URL),
        ];


        return $this->render('yml/sitemap.html.twig', [
            'today' => date("Y-m-d"),
            'products' => $products,
            'pages' => $page_routes,
            'server_name' => sprintf('%s://%s', $_SERVER['REQUEST_SCHEME'], $_SERVER['SERVER_NAME']),
        ]);
    }
}

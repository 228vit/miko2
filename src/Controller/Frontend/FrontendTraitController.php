<?php

namespace App\Controller\Frontend;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

trait FrontendTraitController
{

    private $current_filters = null;
    private $filter_form;
    private $em;
    private $paginator;
    private $query_builder_updater;
    private $countryRepository;
    private $vendorRepository;
    private $foodRepository;
    private $cacheManager;

    public function __construct(EntityManagerInterface $em,
                                PaginatorInterface $paginator,
                                FilterBuilderUpdaterInterface $query_builder_updater,
                                CacheManager $cacheManager)
    {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->query_builder_updater = $query_builder_updater;
        $this->cacheManager = $cacheManager;

    }

    private function getPagination(Request $request,
                                   SessionInterface $session,
                                   $filter_form_class)
    {
        /** @var EntityRepository $repository */
        $repository = $this->em->getRepository(self::NS_ENTITY_NAME);

        $this->filter_form = $this->createForm($filter_form_class, null, array(
            'action' => $this->generateUrl('front_apply_filter', ['model' => self::MODEL]),
            'method' => 'POST',
        ));


        /** @var Query $query */
        $query = $this->buildQuery($repository, $request, $session, $this->filter_form, self::MODEL);

        $pagination = $this->paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            self::ROWS_PER_PAGE  /*limit per page*/
        );

        return $pagination;
    }

    private function buildQuery(EntityRepository $repository, Request $request,
                                SessionInterface $session, FormInterface $filter_form, string $model)
    {
        $sort_by = $request->query->get('sort_by', 'price');
        $order = $request->query->get('order', 'asc');
        $session_filters = $session->get('filters', array());

        if (count($session_filters) && isset($session_filters[$model])) {
            $this->current_filters = $session_filters[$model];
            $this->current_filters['isActive'] = 'y';
            $filter_form->submit($this->current_filters);

            $filterBuilder = $repository->createQueryBuilder($model);

            $this->query_builder_updater
                ->addFilterConditions($filter_form, $filterBuilder)
                ->orderBy($model . '.' . $sort_by, $order);

            $query = $filterBuilder->getQuery();
        } else {
            $this->current_filters = null;
            // default query w/sorting
            $query = $repository->createQueryBuilder($model)
                ->orderBy($model . '.' . $sort_by, $order)
                ->getQuery();
        }

        return $query;
    }

    /**
     * Save filter values in session.
     *
     * @Route("filter/{model}", name="front_apply_filter", methods={"GET", "POST"})
     */
    public function saveModelFilter(Request $request, SessionInterface $session, $model)
    {
        $filter = $request->request->get('item_filter');
        // todo: validate?
        // unset empty values
        if (is_array($filter)) {
            $filter = array_filter($filter, function ($value) {
                return $value !== '';
            });
            // csrf???
            unset($filter['_token']);
        }

        // save filter to session
        $session->set('filters', array(
            $model => $filter,
        ));

        // redirect to referer
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("reset/filter/{model}", name="front_reset_filter")
     */
    public function resetModelFilter(Request $request, SessionInterface $session, $model)
    {
        $current_filters = $session->get('filters', false);
        if (isset($current_filters[$model])) {
            unset($current_filters[$model]);
        }

        $session->set('filters', $current_filters);

        // redirect to referer
        return $this->redirect($request->headers->get('referer'));
    }

}
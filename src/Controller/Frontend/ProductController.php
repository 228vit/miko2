<?php

namespace App\Controller\Frontend;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductImage;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    use FrontendTraitController;

    /**
     * Shows the product entity.
     *
     * @Route("/category/{slug}/id/{id}", name="product_show", methods={"GET", "POST"})
     */
    public function showProduct(Request $request, $slug, $id, EntityManagerInterface $em)
    {
        /** @var CategoryRepository $repository */
        $repository = $em->getRepository(Category::class);
        /** @var Category $category */
        $category = $repository->findOneBy(['slug' => $slug]);
        if (!$category) {
            throw new NotFoundHttpException();
        }

        /** @var Product $product */
        $product = $em->getRepository(Product::class)->find($id);

        if (!$category || !$product) {
            throw new NotFoundHttpException();
        }

        return $this->render('product/show.html.twig', [
            'category' => $category,
            'product' => $product,
        ]);
    }

    /**
     * Генерация витрины товаров на главной стр.
     * @return Response
     */
    public function showOnTop()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var ProductRepository $repo */
        $repo = $em->getRepository(Product::class);

        $rows = $repo->findByShowOnTop();

        return $this->render('product/list.html.twig', [
            'rows' => $rows
        ]);
    }
}
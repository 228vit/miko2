<?php

namespace App\Controller\Frontend;

use App\Entity\Category;
use App\Filter\ProductFilter;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class CategoryController extends AbstractController
{
    use FrontendTraitController;

    CONST ROWS_PER_PAGE = 12;
    CONST MODEL = 'product';
    CONST ENTITY_NAME = 'Product';
    CONST NS_ENTITY_NAME = 'App:Product';

    /**
     * Генерация меню в каталожном LAYOUT
     * @return Response
     */
    public function categoriesTreeMenu(EntityManagerInterface $em)
    {
        $products_count = $em->createQueryBuilder()
            ->select('c.id, COUNT(p.id) as prod_cnt')
            ->from('App:Category', 'c')
            ->innerJoin('c.products', 'p')
            ->where('p.isActive = :isActive')
            ->setParameter('isActive', true)
            ->groupBy('c.id')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

        $category_product_counts = [];

        foreach ($products_count as $item) {
            $category_product_counts[$item['id']] = $item['prod_cnt'];
        }

        $repo = $em->getRepository('App:Category');
        $arrayTree = $repo->childrenHierarchy();

        $tree_arr = [];
        foreach ($arrayTree as $index => $node) {
            if (false === $node['isActive']) {
                continue;
            }
            $tree_arr[$index] = [
                'name' => $node['name'], // . $node['id'],
                'href' => $url = $this->generateUrl("category_show", array(
                        "slug" => $node['slug'])
                ),
            ];
            $tree_arr[$index]['products_cnt'] = isset($category_product_counts[$node['id']]) ? $category_product_counts[$node['id']] : '';
            if (is_array($node['__children']) && sizeof($node['__children'])) {
                $tree_arr[$index]['children'] = $this->renderSubtree($node['__children'], $category_product_counts);
            }
        }

        return $this->render('common/tree_as_menu.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir') . '/..') . DIRECTORY_SEPARATOR,
            'array_tree' => $arrayTree,
            'tree_arr' => $tree_arr,
            'products_count' => $products_count,
        ]);
    }

    private function renderSubtree($arrayTree, $category_product_counts)
    {
        $tree_arr = [];
        foreach ($arrayTree as $index => $node) {
            $tree_arr[] = [
                'name' => $node['name'], // . $node['id'],
                'href' => $url = $this->generateUrl("category_show", array(
                        "slug" => $node['slug'])
                ),
            ];
            $tree_arr[$index]['products_cnt'] = isset($category_product_counts[$node['id']]) ? $category_product_counts[$node['id']] : '';
            if (is_array($node['__children']) && sizeof($node['__children'])) {
                $tree_arr[$index]['children'] = $this->renderSubtree($node['__children'], $category_product_counts);
            }
        }

        return $tree_arr;
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("/category/{slug}", name="category_show", methods={"GET", "POST"})
     * @param Request $request
     * @param $slug
     * @return Response
     */
    public function showCategory(Request $request, SessionInterface $session,
                                 EntityManagerInterface $em, CategoryRepository $repository, $slug)
    {
        /** @var Category $category */
        $category = $repository->findOneBy(['slug' => $slug]);
        if (!$category) {
            throw new NotFoundHttpException();
        }

        // add to session filter category
        $session_filters = $session->get('filters', array());
        $filter = @$session_filters[self::MODEL];
        $filter['category'] = $category->getId();
        $session->set('filters', array(
            self::MODEL => $filter,
        ));

        $pagination = $this->getPagination($request, $session, ProductFilter::class);

        return $this->render('catalog/products.html.twig', array(
            'category' => $category,
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'list_fields' => [
                'a.name' => [
                    'title' => 'Name',
                    'row_field' => 'name',
                    'sorting_field' => 'product.name',
                    'sortable' => true,
                ],
                'a.price' => [
                    'title' => 'Price',
                    'row_field' => 'price',
                    'sorting_field' => 'product.price',
                    'sortable' => true,
                ],

            ]
        ));

//        $page = $request->query->get('page', 1);
//
//        // todo: wrap in try/catch
//
//        $em = $this->getDoctrine()->getManager();
//
//        /** @var Category $category */
//        $category = $em->getRepository(Category::class)->findOneBy(['slug' => $slug]);
//
//        if (!$category) {
//            throw new NotFoundHttpException();
//        }

//        $product_filter = new ProductFilter($request, $slug);
//        $price_from = $product_filter->getPriceFrom();
//        $price_to = $product_filter->getPriceTo();
//        $products = $em->getRepository('App:Product')->getPageFiltered($product_filter);
//        $totalProducts = $em->getRepository('App:Product')->countRowsByFilter($product_filter);
//        $pagesCount = 1;

//        $price_range = $em->getRepository('App:Product')->getMinAndMaxPrice($product_filter);

        return $this->render('catalog/category_as_vitrine.html.twig', [
            'category' => $category,
//            'filter_price_from' => $price_from,
//            'filter_price_to' => $price_to,
//            'price_range' => $price_range,
//            'products' => $products,
//            'totalProducts' => $totalProducts,
//            'pagesCount' => $pagesCount,
            'page' => $page,
        ]);
    }
}
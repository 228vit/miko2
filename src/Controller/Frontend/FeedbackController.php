<?php

namespace App\Controller\Frontend;

use App\Entity\Feedback;
use App\Form\FeedbackType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class FeedbackController extends AbstractController
{
//    /**
//     * @Route("/feedback/success", name="feedback_success", methods={'GET'})
//     */
//    public function success(): Response
//    {
//        return $this->render('feedback/success.html.twig', [
//        ]);
//    }

    public function shortFeedbackForm()
    {
        $feedback = new Feedback();
        $form = $this->createForm(FeedbackType::class, $feedback);

        return $this->render('feedback/footer_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/feedback/create", name="feedback_create")
     * @Method({"GET", "POST"})
     */
    public function create(Request $request, MailerInterface $mailer)
    {
//        $isAjax = $request->isXmlHttpRequest();
//
//        if (!$isAjax) {}
//
//        $feedback = new Feedback();
//        $form = $this->createForm(FeedbackType::class, $feedback);
//
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) { //  && reCaptcha::check()
//
//            $em = $this->getDoctrine()->getManager();
//
//            $em->persist($feedback);
//            $em->flush();
//
//            $email = (new Email())
//                ->from('noreply@mikotek.ru')
//                ->to(new Address('info@mikotek.ru'))
//                ->replyTo(new Address($feedback->getEmail()))
//                ->subject('new feedback #' . $feedback->getId())
//                ->html($this->renderView('emails/mail_new_feedback.html.twig',[
//                        'feedback' => $feedback,
//                    ]
//                ))
//            ;
//
//            $mailer->send($email);
//
//            $this->addFlash('success','Feedback success!');
//
//            return $isAjax ?
//                new JsonResponse(['status' => 'success']) :
//                new RedirectResponse($this->generateUrl('feedback_success'));
//        }
//
//        $this->addFlash('error','Error due creating feedback!');
//
//        return $isAjax ?
//            // todo: extract form errors
//            new JsonResponse(['status' => 'fail'], 400) :
//            $this->render('feedback/new.html.twig', [
//                'form' => $form->createView(),
//            ])
//        ;
    }

}

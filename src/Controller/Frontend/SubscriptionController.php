<?php

namespace App\Controller\Frontend;

use App\Entity\Feedback;
use App\Form\FeedbackType;
use App\Form\SubscriptionType;
use App\Utils\reCaptcha;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SubscriptionController extends AbstractController
{
    /**
     * @Route("/subscription/success", name="subscription_success", methods={"GET"})
     */
    public function success(Request $request)
    {
        return $this->render('subscription/success.html.twig', [
        ]);
    }

    /**
     * @Route("/subscription", name="subscription_new", methods={"GET"})
     */
    public function new(Request $request)
    {
        $form = $this->createForm(SubscriptionType::class);

        return $this->render('subscription/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/subscription", name="subscription_create", methods={"POST"})
     */
    public function create(Request $request, MailerInterface $mailer)
    {
        $form = $this->createForm(SubscriptionType::class);

        $form->handleRequest($request);

//        $recaptcha = $request->request->get('g-recaptcha-response', 'not set');
        if ($form->isSubmitted() && $form->isValid()) {

            // get email from form
            $email = $form->getData()['email'];

            // todo: switch mailer
            $message = (new \Swift_Message('new subscription ' . $email))
                ->setFrom('noreply@mikotek.ru')
                ->setTo('info@mikotek.ru')
                ->setBody($email,'text/plain')
            ;
            $success_cnt = $mailer->send($message);

            $this->addFlash('success','Subscription success!');

            return new RedirectResponse($this->generateUrl('subscription_success'));
        }

        $this->addFlash('error','Error due creating subscription!');

        return $this->render('subscription/new.html.twig', [
                'form' => $form->createView(),
        ]);
    }

}

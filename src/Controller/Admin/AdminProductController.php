<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Entity\ProductImage;
use App\Filter\ProductFilter;
use App\Form\ProductType;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\FileUploader;
use Symfony\Component\Routing\Annotation\Route;

class AdminProductController extends AbstractController
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 10;
    CONST MODEL = 'product';
    CONST ENTITY_NAME = 'Product';
    CONST NS_ENTITY_NAME = 'App:Product';

    /**
     * Lists all product entities.
     *
     * @Route("backend/product/toggle/field", name="ajax_product_toggle_field", methods={"GET"})
     */
    public function ajaxToggleFieldAction(Request $request,
                                          EntityManagerInterface $em,
                                          ProductRepository $repository)
    {
        $id = $request->query->get('id', false);
        $field = $request->query->get('field', false);

        /** @var Product $product */
        $product = $repository->find($id);

        if (false === $product) {
            return new JsonResponse('Wrong ID', 400);
        }

        switch ($field) {
            case 'showOnMarket':
                $status = !$product->isShowOnMarket();
                $product->setShowOnMarket($status);
                break;
            case 'showOnPriceRu':
                $status = !$product->isShowOnPriceRu();
                $product->setShowOnPriceRu($status);
                break;
            case 'isShowOnTop':
                $status = !$product->isShowOnTop();
                $product->setShowOnTop($status);
                break;
            case 'isActive':
                $status = !$product->isActive();
                $product->setIsActive($status);
                break;
            default:
                return new Response('<span class="badge badge-pill badge-secondary"><i class="fa fa-question"></i></span>', 200);
                break;
        }

        $em->persist($product);
        $em->flush();

        $response = sprintf('<span class="badge badge-pill badge-%s"><i class="fa fa-%s"></i></span>',
            ($status ? 'success': 'danger'),
            ($status ? 'check-square': 'window-close')
        );

        return new Response($response, 200);
    }

    /**
     * Lists all product entities.
     *
     * @Route("backend/product/change/status", name="ajax_product_status", methods={"GET"})
     */
    public function ajaxStatusAction(Request $request,
                                     EntityManagerInterface $em,
                                     ProductRepository $repository)
    {
        $id = $request->query->get('id', false);

        $product = $repository->find($id);

        if (false === $product) {
            return new JsonResponse('Wrong ID', 400);
        }

        $status = $request->query->get('status');
        $product->setStatus($status);

        $em->persist($product);
        $em->flush();

        return new JsonResponse('ok', 200);
    }

    /**
     * @Route("backend/pricelist", name="backend_pricelist", methods={"GET"})
     */
    public function pricelist(CategoryRepository $categoryRepository)
    {
        $this->filter_form = $this->createForm(ProductFilter::class, null, array(
            'action' => $this->generateUrl('backend_apply_filter', ['model' => self::MODEL]),
            'method' => 'POST',
        ));

        $categories = $categoryRepository->pricelist(['ram', 'ssd', 'desktop-cpu', 'server-cpu']);

        return $this->render('admin/product/pricelist.html.twig', array(
            'filter_form' => $this->filter_form->createView(),
            'categories' => $categories,
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'statuses' => Product::getStatuses(),
        ));
    }

    /**
     * Lists all product entities.
     *
     * @Route("backend/product/index", name="backend_product_index", methods={"GET"})
     */
    public function indexAction(Request $request, SessionInterface $session)
    {
        $pagination = $this->getPagination($request, $session, ProductFilter::class);

        return $this->render('admin/product/index.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'statuses' => Product::getStatuses(),
            'list_fields' => [
                'a.id' => [
                    'title' => 'ID',
                    'row_field' => 'id',
                    'sorting_field' => 'product.id',
                    'sortable' => true,
                ],
                'a.name' => [
                    'title' => 'Name',
                    'row_field' => 'name',
                    'sorting_field' => 'product.name',
                    'sortable' => true,
                ],
                'a.price' => [
                    'title' => 'Price',
                    'row_field' => 'price',
                    'sorting_field' => 'product.price',
                    'sortable' => true,
                ],
                'a.pic' => [
                    'title' => 'Pic',
                    'row_field' => 'pic',
                    'sorting_field' => 'product.pic',
                    'sortable' => false,
                ],
                'a.isActive' => [
                    'title' => 'Is active?',
                    'row_field' => 'isActive',
                    'sorting_field' => 'product.isActive',
                    'sortable' => false,
                ],
            ]
        ));
    }


    /**
     * Creates a new product entity.
     *
     * @Route("backend/product/new", name="backend_product_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request,
                              EntityManagerInterface $em,
                              FileUploader $fileUploader)
    {
//        $this->denyAccessUnlessGranted('ROLE_SUPERADMIN', null, 'Unable to access this page!');

        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $product->getPicFile();

            if (null !== $file) {
                $fileName = $fileUploader->upload($file);
                $product->setPic($fileName);
            }

            $em->persist($product);
            $em->flush();

            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('backend_product_edit', array('id' => $product->getId()));
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/common/new.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,

        ));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("backend/product/{id}/clone", name="backend_product_clone", methods={"GET"})
     */
    public function cloneAction(Product $product,
                                EntityManagerInterface $em,
                                ProductRepository $repository)
    {
        /** @var Product $new_product */
        $new_product = clone $product;
        $new_product->setName('clone ' . $product->getName());

        $em->persist($new_product);
        $em->flush();

        $this->addFlash('success', 'Product cloned successfully!');
        return $this->redirectToRoute('backend_product_edit', array('id' => $new_product->getId()));
    }


    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("backend/products/update", name="backend_products_update", methods={"POST"})
     */
    public function massUpdate(Request $request,
                               EntityManagerInterface $em,
                               ProductRepository $repository)
    {
        $items = $request->request->get('product');

        foreach ($items as $id => $item) {
            /** @var Product $product */
            $product = $repository->find($id);
            if (false === $product) {
                continue;
            }
            $product
                ->setPrice($item['price'])
                ->setPriceOpt(intval($item['price_opt']))
                ->setPriceNal(intval($item['price_nal']))
                ->setPriceUSD(floatval($item['price_usd']))
                ->setStockQty(empty($item['stockQty']) ? null : intval($item['stockQty']))
            ;

            $em->persist($product);
        }
        $em->flush();

        $this->addFlash('success', 'Your changes were saved!');

        // redirect to referer
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("backend/product/{id}/edit", name="backend_product_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Product $product, FileUploader $fileUploader)
    {
        $deleteForm = $this->createDeleteForm($product);
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /*
             * 1. Если в форме оставить только pic, то при отправке формы без фото, поле pic стирается
             * 2. Если добавить picFile, которые не хранится в базе, то получается что при изменении только
             * фото, модель не считается изменённой, и вообще не сохраняется, не срабатывают Listeners
             * как вариант, в таком случае менять UpdatedAt, и дальше пусть работает Listener
             */
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $product->getPicFile();

            if (null !== $file) {
                $fileName = $fileUploader->upload($file);
                $product->setPic($fileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $this->addFlash('success', 'Your changes were saved!');
//            return $this->redirectToRoute('backend_product_edit', array('id' => $product->getId()));
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due saving object!');
        }

        return $this->render('admin/product/edit.html.twig', array(
            'row' => $product,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }


    /**
     * Finds and displays a product entity.
     *
     * @Route("backend/product/{id}", name="backend_product_show", methods={"GET"})
     */
    public function showAction(Product $product)
    {
        $deleteForm = $this->createDeleteForm($product);

        return $this->render('admin/product/show.html.twig', array(
            'product' => $product,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a product entity.
     *
     * @Route("backend/product/{id}", name="backend_product_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Product $product, EntityManagerInterface $em)
    {
        $filter_form = $this->createDeleteForm($product);
        $filter_form->handleRequest($request);

        if ($filter_form->isSubmitted() && $filter_form->isValid()) {
            $em->remove($product);
            $em->flush();

            $this->addFlash('success', 'Record was successfully deleted!');
        }

        if (!$filter_form->isValid()) {
            /** @var FormErrorIterator $errors */
            $errors = $filter_form->getErrors()->__toString();
            $this->addFlash('danger', 'Error due deletion! ' . $errors);
        }

        return $this->redirectToRoute('backend_product_index');
    }

    /**
     * Finds and displays a product entity.
     *
     * @Route("backend/product_set_taiwan", name="backend_product_set_taiwan", methods={"GET"})
     */
    public function setTaiwan(ProductRepository $repository)
    {
        $repository->setCountry('Тайвань');

        $this->addFlash('success', 'Taiwan');

        return $this->redirectToRoute('backend_product_index');
    }

    /**
     * Creates a form to delete a product entity.
     *
     * @param Product $product The product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_product_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}

<?php

namespace App\Controller\Admin;

use App\Entity\Faq;
use App\Filter\FaqFilter;
use App\Form\FaqType;
use App\Repository\FaqRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class AdminFaqController extends AbstractController
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 10;
    CONST MODEL = 'faq';
    CONST ENTITY_NAME = 'Faq';
    CONST NS_ENTITY_NAME = 'App:Faq';

    /**
     * Lists all faq entities.
     *
     * @Route("backend/faq/index", name="backend_faq_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, SessionInterface $session)
    {
        $pagination = $this->getPagination($request, $session, FaqFilter::class);

        return $this->render('admin/common/index.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'list_fields' => [
                'a.id' => [
                    'title' => 'ID',
                    'row_field' => 'id',
                    'sorting_field' => 'faq.id',
                    'sortable' => true,
                ],
                'a.author' => [
                    'title' => 'Author',
                    'row_field' => 'author',
                    'sorting_field' => 'faq.author',
                    'sortable' => true,
                ],
                'a.question' => [
                    'title' => 'Question',
                    'row_field' => 'question',
                    'sorting_field' => 'faq.question',
                    'sortable' => false,
                ],
                'a.answer' => [
                    'title' => 'Answer',
                    'row_field' => 'answer',
                    'sorting_field' => 'faq.answer',
                    'sortable' => false,
                ],
            ]
        ));
    }


    /**
     * Creates a new faq entity.
     *
     * @Route("backend/faq/new", name="backend_faq_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERADMIN', null, 'Unable to access this page!');

        $faq = new Faq();
        $form = $this->createForm('App\Form\FaqType', $faq);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($faq);
            $em->flush();
            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('backend_faq_edit', array('id' => $faq->getId()));
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/common/new.html.twig', array(
            'faq' => $faq,
            'form' => $form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,

        ));
    }

    /**
     * Finds and displays a faq entity.
     *
     * @Route("backend/faq/{id}", name="backend_faq_show", methods={"GET"})
     */
    public function showAction(Faq $faq)
    {
        $deleteForm = $this->createDeleteForm($faq);

        return $this->render('admin/faq/show.html.twig', array(
            'faq' => $faq,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing faq entity.
     *
     * @Route("backend/faq/{id}/edit", name="backend_faq_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Faq $faq, EntityManagerInterface $em)
    {
        $deleteForm = $this->createDeleteForm($faq);
        $editForm = $this->createForm(FaqType::class, $faq);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->addFlash('success', 'Your changes were saved!');
            $em->flush();

            return $this->redirectToRoute('backend_faq_edit', array('id' => $faq->getId()));
        }
        if ($editForm->isSubmitted() && !$editForm->isValid()) {
            $this->addFlash('danger', 'Errors due saving object!');
        }

        return $this->render('admin/common/edit.html.twig', array(
            'row' => $faq,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }

    /**
     * Deletes a faq entity.
     *
     * @Route("backend/faq/{id}", name="backend_faq_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Faq $faq, EntityManagerInterface $em)
    {
        $filter_form = $this->createDeleteForm($faq);
        $filter_form->handleRequest($request);

        if ($filter_form->isSubmitted() && $filter_form->isValid()) {
            $em->remove($faq);
            $em->flush();

            $this->addFlash('success', 'Record was successfully deleted!');
        }

        if (!$filter_form->isValid()) {
            /** @var FormErrorIterator $errors */
            $errors = $filter_form->getErrors()->__toString();
            $this->addFlash('danger', 'Error due deletion! ' . $errors);
        }

        return $this->redirectToRoute('backend_faq_index');
    }

    /**
     * Creates a form to delete a faq entity.
     *
     * @param Faq $faq The faq entity
     *
     * @return \Symfony\Component\Form\FormInterface The form
     */
    private function createDeleteForm(Faq $faq)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_faq_delete', array('id' => $faq->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


}

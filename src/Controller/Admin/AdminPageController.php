<?php

namespace App\Controller\Admin;

use App\Entity\Page;
use App\Filter\PageFilter;
use App\Form\PageType;
use App\Repository\PageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Page controller.
 */
class AdminPageController extends AbstractController
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 10;
    CONST MODEL = 'page';
    CONST ENTITY_NAME = 'Page';
    CONST NS_ENTITY_NAME = 'App:Page';

    /**
     * Lists all page entities.
     *
     * @Route("backend/page/index", name="backend_page_index", methods={"GET"})
     */
    public function indexAction(Request $request, SessionInterface $session, EntityManagerInterface $em,
                                PageRepository $repository, PaginatorInterface $paginator)
    {
        $filter_form = $this->createForm(PageFilter::class, null, array(
            'action' => $this->generateUrl('backend_apply_filter', ['model' => self::MODEL]),
            'method' => 'POST',
        ));

        /** @var Query $query */
        $query = $this->buildQuery($repository, $request, $session, $filter_form, self::MODEL);

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            self::ROWS_PER_PAGE  /*limit per page*/
        );

        $pages = $query->getResult();

        return $this->render('admin/page/index_new.html.twig', array(
            'pages' => $pages,
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,

        ));
    }

    /**
     * Creates a new page entity.
     *
     * @Route("backend/page/new", name="backend_page_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {
        $page = new Page();
        $form = $this->createForm('App\Form\PageType', $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($page);
            $em->flush();
            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('backend_page_edit', array('id' => $page->getId()));
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/page/new.html.twig', array(
            'page' => $page,
            'form' => $form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }

    /**
     * Finds and displays a page entity.
     *
     * @Route("backend/page/{id}", name="backend_page_show", methods={"GET"})
     */
    public function showAction(Page $page)
    {
        $deleteForm = $this->createDeleteForm($page);

        return $this->render('admin/page/show.html.twig', array(
            'page' => $page,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing page entity.
     *
     * @Route("backend/page/{id}/edit", name="backend_page_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Page $page, EntityManagerInterface $em)
    {
        $deleteForm = $this->createDeleteForm($page);
        $editForm = $this->createForm(PageType::class, $page);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();
            $this->addFlash('success', 'Your changes were saved!');

            return $this->redirectToRoute('backend_page_edit', array('id' => $page->getId()));
        }
        if ($editForm->isSubmitted() && !$editForm->isValid()) {
            $this->addFlash('danger', 'Errors due saving object!');
        }

        return $this->render('admin/page/edit.html.twig', array(
            'page' => $page,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }

    /**
     * Deletes a page entity.
     *
     * @Route("backend/page/{id}", name="backend_page_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Page $page, EntityManagerInterface $em)
    {
        $filter_form = $this->createDeleteForm($page);
        $filter_form->handleRequest($request);

        if ($filter_form->isSubmitted() && $filter_form->isValid()) {
            $em->remove($page);
            $em->flush();

            $this->addFlash('success', 'Record was successfully deleted!');
        }

        if (!$filter_form->isValid()) {
            /** @var FormErrorIterator $errors */
            $errors = $filter_form->getErrors()->__toString();
            $this->addFlash('danger', 'Error due deletion! ' . $errors);
        }

        return $this->redirectToRoute('backend_page_index');
    }

    /**
     * Creates a form to delete a page entity.
     *
     * @param Page $page The page entity
     *
     * @return \Symfony\Component\Form\FormInterface The form
     */
    private function createDeleteForm(Page $page)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_page_delete', array('id' => $page->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


}

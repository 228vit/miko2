<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function findAllJoinProducts(): array
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.Products p')
            ->where('p.isActive = :isActive')
            ->setParameter('isActive', true)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->execute()
        ;
    }

    public function pricelist(array $slugs): array
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.products','p')
            ->addSelect(('p'))
            ->where('c.slug IN (:slugs)')
            ->setParameter('slugs', $slugs)
            ->andWhere('p.isActive = :true')
            ->setParameter('true', true)
            ->orderBy('p.name', 'ASC')
            ->getQuery()
            ->execute()
        ;
    }
}

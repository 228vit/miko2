<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findByShowOnTop()
    {
        return $this->createQueryBuilder('p')
            ->where('p.showOnTop = :show_on_top')
            ->setParameter('show_on_top', true)
            ->innerJoin('p.category', 'c')
//            ->orderBy('p.id', 'ASC')
            ->setMaxResults(6)
            ->getQuery()
            ->getResult()
        ;
    }

    public function setCountry($country)
    {
        return $this->createQueryBuilder('p')
            ->update()
            ->set('p.countryOfOrigin', ':c')
            ->setParameter('c', $country)
            ->getQuery()
            ->execute()
        ;
    }

}

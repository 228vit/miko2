<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Vendor;
use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('vendor', EntityType::class, array(
                'class' => Vendor::class,
                'choice_label' => 'name',
                'required' => false,
                'expanded' => false,
            ))
            ->add('vendorCode')
            ->add('vendorModel')
            ->add('sku')
            ->add('countryOfOrigin', TextType::class, [
                'label' => 'Страна происх.'
            ])
            ->add('category', EntityType::class, array(
                'class' => 'App:Category',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->addOrderBy('c.root', 'ASC')
                        ->addOrderBy('c.lft', 'ASC');
                },
                'choice_label' => 'forTree',
                'expanded' => false,
            ))

            ->add('name')
            ->add('fullName')
            ->add('price')
            ->add('priceOpt')
            ->add('stockQty', IntegerType::class, [
                'label' => 'Кол-во на складе',
            ])
            ->add('warranty')
            ->add('announce', TextareaType::class, [
                'attr' => [
                    'rows' => 6,
                ]
            ])
            ->add('description', CKEditorType::class, [
                    'config' => [
                        'uiColor' => '#ffffff',
                        //...
                    ],
                ]
            )
            ->add('picFile', FileType::class, array(
                'label' => 'Image file',
                'data_class' => null,
                'required' => false
            ))
            ->add('isActive')
            ->add('showOnMarket', CheckboxType::class, ['required' => false])
            ->add('showOnPriceRu', CheckboxType::class, ['required' => false])
            ->add('showOnTop')
            ->add('status', ChoiceType::class, [
                'choices'  => Product::getFormStatuses(),
                'expanded' => true,
            ])
        ;


//        $builder->add('images', CollectionType::class, [
//            'entry_type' => ProductImageType::class,
//            'entry_options' => ['label' => false],
//            'allow_add' => true,
//        ]);

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Product::class
        ));
    }

    /**
     * {@inheritdoc}
     */
//    public function getBlockPrefix()
//    {
//        return 'product';
//    }


}

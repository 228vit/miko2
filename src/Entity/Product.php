<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    CONST STATUS_STOCK = 'stock';
    CONST STATUS_TRANSIT = 'transit';
    CONST STATUS_RESERVE = 'reserve';
    CONST STATUS_ARCHIVE = 'archive';
    CONST STATUS_PREORDER = 'preorder';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Category|null the group this user belongs (if any)
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var Vendor|null the group this user belongs (if any)
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Vendor", inversedBy="products")
     * @ORM\JoinColumn(name="vendor_id", referencedColumnName="id")
     */
    private $vendor;

    /**
     * @ORM\Column(length=512)
     */
    private $name;

    /**
     * @ORM\Column(length=512)
     */
    private $full_name;

    /**
     * @ORM\Column(length=128)
     */
    private $sku;

    /**
     * @ORM\Column(length=128)
     */
    private $vendorModel;

    /**
     * @ORM\Column(length=128)
     */
    private $vendorCode;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="price_opt", type="integer")
     */
    private $priceOpt;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $priceUSD;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priceNal;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, options={"default" : "12 мес."})
     */
    private $warranty;

    /**
     * @var string
     *
     * @ORM\Column(name="announce", type="text")
     */
    private $announce;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $pic;

    /**
     * @var File
     *
     * @Assert\File(mimeTypes={ "image/jpg", "image/jpeg", "image/png" })
     */
    private $picFile;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var bool
     *
     * @ORM\Column(name="show_on_top", type="boolean")
     */
    private $showOnTop;

    /**
     * @var bool
     *
     * @ORM\Column(name="show_on_market", type="boolean", options={"default": "0"})
     */
    private $showOnMarket;

    /**
     * @var bool
     *
     * @ORM\Column(name="show_on_price_ru", type="boolean", options={"default": "0"})
     */
    private $showOnPriceRu;

    /**
     * @var string
     *
     * @ORM\Column(type="string", columnDefinition="ENUM('stock', 'transit', 'reserve', 'preorder', 'archive')")
     */
    private $status;

    /**
     * One Product has Many Images.
     * @ORM\OneToMany(targetEntity="ProductImage", mappedBy="product", indexBy="id")
     */
    private $images;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stockQty;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $countryOfOrigin;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Product
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category|null $category
     * @return Product
     */
    public function setCategory(Category $category): Product
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return Product
     */
    public function setPrice(int $price): Product
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Product
     */
    public function setDescription(string $description): Product
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getWarranty()
    {
        return $this->warranty;
    }

    /**
     * @param string $warranty
     * @return Product
     */
    public function setWarranty(string $warranty): Product
    {
        $this->warranty = $warranty;
        return $this;
    }

    /**
     * @return string
     */
    public function getPic()
    {
        return $this->pic;
    }

    /**
     * @param string $pic
     * @return Product
     */
    public function setPic($pic)
    {
        $this->pic = $pic;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return Product
     */
    public function setIsActive(bool $isActive): Product
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowOnTop()
    {
        return $this->showOnTop;
    }

    /**
     * @param bool $showOnTop
     * @return Product
     */
    public function setShowOnTop(bool $showOnTop): Product
    {
        $this->showOnTop = $showOnTop;
        return $this;
    }

    /**
     * @return File
     */
    public function getPicFile()
    {
        return $this->picFile;
    }

    /**
     * @param string $picFile
     * @return Product
     */
    public function setPicFile(File $picFile): Product
    {
        $this->picFile = $picFile;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnnounce()
    {
        return $this->announce;
    }

    /**
     * @param string $announce
     * @return Product
     */
    public function setAnnounce(string $announce): Product
    {
        $this->announce = $announce;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVendorModel()
    {
        return $this->vendorModel;
    }

    /**
     * @param mixed $vendorModel
     * @return Product
     */
    public function setVendorModel($vendorModel)
    {
        $this->vendorModel = $vendorModel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVendorCode()
    {
        return $this->vendorCode;
    }

    /**
     * @param mixed $vendorCode
     * @return Product
     */
    public function setVendorCode($vendorCode)
    {
        $this->vendorCode = $vendorCode;
        return $this;
    }

    /**
     * @return Vendor|null
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param Vendor|null $vendor
     * @return Product
     */
    public function setVendor(Vendor $vendor): Product
    {
        $this->vendor = $vendor;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status ?: Product::STATUS_STOCK;
    }

    /**
     * @param string $status
     * @return Product
     */
    public function setStatus(string $status): Product
    {
        $this->status = $status;
        return $this;
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_STOCK => 'на складе',
            self::STATUS_TRANSIT =>'в транзите',
            self::STATUS_RESERVE => 'в резерве',
            self::STATUS_PREORDER => 'под заказ',
            self::STATUS_ARCHIVE => 'снят с производства',
        ];
    }

    public static function getFormStatuses()
    {
        return [
            'на складе' => self::STATUS_STOCK,
            'в транзите' => self::STATUS_TRANSIT,
            'в резерве' => self::STATUS_RESERVE,
            'под заказ' => self::STATUS_PREORDER,
            'снят с производства' => self::STATUS_ARCHIVE,
        ];
    }

    public function statusAsCssClass()
    {
        $css_classes = [
            self::STATUS_STOCK => 'success',
            self::STATUS_TRANSIT => 'primary',
            self::STATUS_PREORDER => 'danger',
            self::STATUS_RESERVE => 'danger',
            self::STATUS_ARCHIVE => 'dark',
        ];

        return $css_classes[$this->getStatus()];
    }

    public function statusRus()
    {
        $statuses = self::getStatuses();

        return $statuses[$this->getStatus()];
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * @param mixed $full_name
     * @return Product
     */
    public function setFullName($full_name)
    {
        $this->full_name = $full_name;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriceOpt()
    {
        return $this->priceOpt;
    }

    /**
     * @param int $priceOpt
     * @return Product
     */
    public function setPriceOpt(int $priceOpt): Product
    {
        $this->priceOpt = $priceOpt;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowOnMarket()
    {
        return (bool)$this->showOnMarket;
    }

    /**
     * @param bool $showOnMarket
     * @return Product
     */
    public function setShowOnMarket(bool $showOnMarket): Product
    {
        $this->showOnMarket = $showOnMarket;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowOnPriceRu()
    {
        return (bool)$this->showOnPriceRu;
    }

    /**
     * @param bool $showOnPriceRu
     * @return Product
     */
    public function setShowOnPriceRu(bool $showOnPriceRu): Product
    {
        $this->showOnPriceRu = $showOnPriceRu;
        return $this;
    }


    public function isAvailableToOrder()
    {
        return in_array($this->status, [
            self::STATUS_STOCK,
            self::STATUS_PREORDER
        ]);
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku(?string $sku): void
    {
        $this->sku = $sku;
    }

    public function getStockQty(): ?int
    {
        return $this->stockQty;
    }

    public function setStockQty(?int $stockQty): self
    {
        $this->stockQty = $stockQty;

        return $this;
    }

    public function getCountryOfOrigin(): ?string
    {
        return $this->countryOfOrigin;
    }

    public function setCountryOfOrigin(?string $countryOfOrigin): self
    {
        $this->countryOfOrigin = $countryOfOrigin;

        return $this;
    }

    public function getPriceUSD(): ?float
    {
        return $this->priceUSD;
    }

    public function setPriceUSD(?float $priceUSD): self
    {
        $this->priceUSD = $priceUSD;

        return $this;
    }

    public function getPriceNal(): ?int
    {
        return $this->priceNal;
    }

    public function setPriceNal(?int $priceNal): self
    {
        $this->priceNal = $priceNal;

        return $this;
    }


}
